import { Length } from "class-validator";
import { Field, InputType } from "type-graphql";

@InputType()
export default class ResetPassswordInput {
  @Field()
  token: string;

  @Field()
  @Length(8, 64)
  password: string;
}
