import { Resolver, Query, Ctx, Authorized } from "type-graphql";
import { User } from "../models/User";
import { Context } from "../utils";

@Resolver()
export default class UserResolver {
  @Authorized()
  @Query((returns) => User)
  async me(@Ctx() ctx: Context): Promise<User> {
    return await ctx.db.user.findFirst({
      where: { id: ctx.currentUserId },
    });
  }
}
