import { Resolver, Ctx, Arg, Mutation } from "type-graphql";

import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import crypto from "crypto";

import { Context, sleep } from "../utils";
import { ResetPassswordInput } from "../inputs";

const RESET_PASSWORD_TOKEN_EXPIRATION_IN_HOURS = 2;

@Resolver()
export default class AuthResolver {
  @Mutation((returns) => String)
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() ctx: Context
  ): Promise<string> {
    const user = await ctx.db.user.findFirst({
      where: {
        email,
      },
    });

    if (!user) {
      throw new Error(`No such user found for email: ${email}`);
    }

    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
      throw new Error("Invalid password");
    }

    return jwt.sign({ userId: user.id }, process.env.APP_SECRET, {
      expiresIn: "30m",
    });
  }

  @Mutation((returns) => Boolean)
  async forgotPassword(
    @Arg("email") email: string,
    @Ctx() ctx: Context
  ): Promise<boolean> {
    const user = await ctx.db.user.findFirst({
      where: {
        email,
      },
    });

    const token = crypto.randomBytes(64).toString("base64");

    const expiresAt = new Date();
    expiresAt.setHours(
      expiresAt.getHours() + RESET_PASSWORD_TOKEN_EXPIRATION_IN_HOURS
    );

    if (user) {
      await ctx.db.token.deleteMany({
        where: { type: "RESET_PASSWORD", userId: user.id },
      });

      await ctx.db.token.create({
        data: {
          user: { connect: { id: user.id } },
          type: "RESET_PASSWORD",
          expiresAt,
          value: token,
          sentTo: user.email,
        },
      });

      // TO-DO: Send e-mail with token
    } else {
      // Wait to ensure that response times in both cases are similar
      await sleep(20);
    }

    // Return success value regardless of whether account exists or not
    return true;
  }

  @Mutation((returns) => Boolean)
  async resetPassword(
    @Arg("input") input: ResetPassswordInput,
    @Ctx() ctx: Context
  ): Promise<boolean> {
    const { token, password } = input;

    const matchingToken = await ctx.db.token.findFirst({
      where: {
        value: token,
        type: "RESET_PASSWORD",
        expiresAt: { gte: new Date() },
      },
    });

    if (!matchingToken) {
      throw new Error("Token is invalid or has expired.");
    }

    await ctx.db.token.delete({ where: { id: matchingToken.id } });

    const newPassword = await bcrypt.hash(password, 10);

    const user = await ctx.db.user.update({
      where: { id: matchingToken.userId },
      data: { password: newPassword },
    });

    return true;
  }
}
