import { ObjectType, Field } from "type-graphql";

@ObjectType()
export class User {
  @Field()
  id: number;

  @Field()
  createdAt: Date;

  @Field()
  email: string;

  @Field()
  firstName: string;

  @Field()
  lastName: string;
}
