import { PrismaClient } from "@prisma/client";
import { ExpressContext } from "apollo-server-express";
import { AuthChecker } from "type-graphql";

import * as jwt from "jsonwebtoken";

export interface Context extends ExpressContext {
  currentUserId: number;
  db: PrismaClient;
}

export const authChecker: AuthChecker<Context> = ({
  context: { currentUserId },
}) => {
  return !!currentUserId;
};

export function verifyJwt(req, res, next) {
  const authorizationHeader = req.headers.authorization || "";

  if (authorizationHeader) {
    const [, token] = authorizationHeader.split(" ");

    const { userId } = jwt.verify(token, process.env.APP_SECRET) as {
      userId: string;
    };

    req.userId = parseInt(userId);
  }

  next();
}

export function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
