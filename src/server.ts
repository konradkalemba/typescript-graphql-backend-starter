import "reflect-metadata";

import express from "express";
import helmet from "helmet";
import { ApolloServer } from "apollo-server-express";

import { buildSchema } from "type-graphql";
import { AuthResolver, UserResolver } from "./resolvers";

import { PrismaClient } from "@prisma/client";
import { Context, authChecker, verifyJwt } from "./utils";

const app = express();
const db = new PrismaClient();

app.use(helmet({ contentSecurityPolicy: false }));

void (async function start() {
  const schema = await buildSchema({
    resolvers: [AuthResolver, UserResolver],
    authChecker,
  });

  const server = new ApolloServer({
    schema,
    context: (ctx): Context => {
      return {
        ...ctx,
        currentUserId: ctx.req.userId,
        db,
      };
    },
    tracing: true,
  });

  app.use(server.graphqlPath, verifyJwt);

  server.applyMiddleware({ app });

  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  );
})();
